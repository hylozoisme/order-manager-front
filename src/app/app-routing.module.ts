import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./modules/home/home.component";
import {LoginComponent} from "./modules/login/login.component";
import {PanierComponent} from "./modules/panier/panier.component";


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'connexion',
    component: LoginComponent,
  },
  {
    path: 'panier',
    component: PanierComponent,
  },
  {
    //404
    path: '**',
    redirectTo: ''
  },
  {
    //500
    path: 'error',
    redirectTo: ''
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
