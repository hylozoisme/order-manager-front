import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './modules/home/home.component';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import { LoginComponent } from './modules/login/login.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatInputModule } from "@angular/material/input";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AgendaCardComponent } from './shared/components/agenda-card/agenda-card.component';
import { AgendaComponent } from './modules/agenda/agenda.component';
import { AddContactComponent } from './shared/components/add-contact/add-contact.component';
import { AddAgendaComponent } from './shared/components/add-agenda/add-agenda.component';
import { PanierComponent } from './modules/panier/panier.component';
import { PanierItemComponent } from './shared/components/panier-item/panier-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    AgendaCardComponent,
    AgendaComponent,
    AddContactComponent,
    AddAgendaComponent,
    PanierComponent,
    PanierItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatInputModule,
    HttpClientModule,
    MatCheckboxModule,
    FontAwesomeModule,
    FormsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
