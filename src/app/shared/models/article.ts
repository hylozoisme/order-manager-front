export interface Article {
  name: string;
  unitPrice: number;
  quantity: number;
}
