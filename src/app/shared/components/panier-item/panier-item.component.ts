import {Component, Input} from '@angular/core';
import {Article} from "../../models/article";

@Component({
  selector: 'app-panier-item',
  templateUrl: './panier-item.component.html',
  styleUrls: ['./panier-item.component.scss']
})
export class PanierItemComponent {
  @Input() item: Article | undefined;

  constructor() { }

  ngOnInit(): void { }
}
