import {Component, Input, ViewChild} from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { AgendaService } from "../../../core/services/agenda.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent {
  faTimes = faTimes;
  modalOpen = false;
  form!: FormGroup;
  @ViewChild('validateButton') validateButton!: any;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private agendaService: AgendaService)
  { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      unitPrice: ['', [Validators.required]],
      quantity: ['', [Validators.required]],
    });

    this.agendaService.articleClicked$.subscribe((contact: any) => {
      this.agendaService.openModal();
      console.log(contact);
    });

    this.agendaService.isModalOpen().subscribe((modalOpen: boolean) => {
      this.modalOpen = modalOpen;
    });
  }


  closeModal() {
    this.agendaService.closeModal();
  }

  addArticle() {

    let article = {
      name: this.form.get('name')?.value,
      unitPrice: this.form.get('unitPrice')?.value,
      quantity: this.form.get('quantity')?.value,
    }

    this.agendaService.createArticle(article);
  }
}
