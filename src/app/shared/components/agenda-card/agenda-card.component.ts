import {Component, Input} from '@angular/core';
import { Article } from "../../models/article";
import {PanierService} from "../../../core/services/panier.service";

@Component({
  selector: 'app-agenda-card',
  templateUrl: './agenda-card.component.html',
  styleUrls: ['./agenda-card.component.scss']
})
export class AgendaCardComponent {
  @Input('article') article: Article | undefined;

  constructor(public panierService: PanierService) { }
}
