import { Component, HostListener, OnInit } from '@angular/core';
import {AuthService} from "../authentification/auth.service";
import { LoggedUser } from "../../shared/models/loggedUser";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  isScroll: boolean = false;
  menuOpen: boolean = false;
  loggedUser!: LoggedUser;
  initials: string = '';
  @HostListener('window:scroll', [])
  onWindowScroll() {
    const scrollPosition = window.scrollY || document.documentElement.scrollTop || document.body.scrollTop || 0;
    this.isScroll = scrollPosition >= 20;
  }

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void { }

  logout(): void {
    console.log('logout');
  }

  toggleMenu() {
    this.menuOpen = !this.menuOpen;
  }
}
