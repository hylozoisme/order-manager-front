import { Injectable } from '@angular/core';
import { User } from "../../shared/models/user";
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { api } from "../api";

interface JwtPayload {
  exp: number;
}


@Injectable({
  providedIn: 'root',
})

export class AuthService {
  endpoint: string = api;
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient, public router: Router) { }

  // Sign-up
  signUp(user: User): Observable<any> {
    let api = `${this.endpoint}/register-user`;
    return this.http.post(api, user).pipe(catchError(this.handleError));
  }

  // Sign-in
  signIn(username: string, password: string) {
    localStorage.setItem('access_token',
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4g' +
      'RG9lIiwiaWF0IjoyNTE2MjM5MDIyfQ.G7efBSFEsClWiTQaJ-r4ApV25eGzy6fcKFuVUVhT8ic');
    localStorage.setItem('refresh_token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwI' +
      'iwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoyNTE2MjM5MDIyfQ.G7efBSFEsClWiTQaJ-r4ApV25eGzy6fcKFuVUVhT8ic');

    this.router.navigate(['/']);
  }

  validateToken(token: string): boolean {
    try {
      const decoded: JwtPayload = jwt_decode(token);
      const currentTime = Date.now() / 1000;
      if (decoded && decoded.exp && decoded.exp > currentTime) {
        return true;
      }
    } catch (error) {
      console.error(error);
    }
    return false;
  }

  getToken() {
    return localStorage.getItem('access_token');
  }

  getRefreshToken(){
    return localStorage.getItem('refresh_token');
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('access_token');
    return authToken !== null;
  }

  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }
}
