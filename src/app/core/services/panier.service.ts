import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {api} from "../api";

@Injectable({
  providedIn: 'root'
})
export class PanierService {
  panier: any[] = [];
  message: string = '';

  constructor(private http: HttpClient) { }

  addArticle(article: any) {
    this.panier?.push(article);
    console.log(this.panier);
    console.log(article);
  }

  getPanier() {
    return this.panier;
  }

  validateOrder() {
    this.http.post(api + '/commandes/create', { "customerName": 'Bob', "orderItems": this.panier }).subscribe(
      (data) => {
        console.log(data);
      }, (error) => {
        this.panier = [];
        this.message = error.error.text;
    })
  }
}
