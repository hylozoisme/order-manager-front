import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {api} from "../api";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private articleClicked = new Subject<number>();
  articleClicked$ = this.articleClicked.asObservable();

  private modalOpen = false;
  private modalOpenSubject = new Subject<boolean>();
  modalOpen$ = this.modalOpenSubject.asObservable();

  constructor(private http: HttpClient) {
  }

  openModal() {
    this.modalOpen = true;
    this.modalOpenSubject.next(true);
  }

  closeModal() {
    this.modalOpen = false;
    this.modalOpenSubject.next(false);
  }

  isModalOpen(): Observable<boolean> {
    return this.modalOpenSubject.asObservable();
  }

  clickArticle(article: any) {
    this.articleClicked.next(article);
  }

  getArticle(id: string){
    return this.http.get(api + '/articles/' + id);
  }

  userLearnedArticle(article: any) {
    return this.http.post(api + '/articles/' + article.id + '/user_learned', {});
  }
}
