import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {Article} from "../../shared/models/article";
import { HttpClient } from "@angular/common/http";
import {api} from "../api";

@Injectable({
  providedIn: 'root'
})
export class AgendaService {

  agenda: Article[] = [];

  private articleClicked = new Subject<number>();
  articleClicked$ = this.articleClicked.asObservable();

  private modalOpen = false;
  private modalOpenSubject = new Subject<boolean>();
  modalOpen$ = this.modalOpenSubject.asObservable();

  constructor(private http: HttpClient) { }

  openModal() {
    this.modalOpen = true;
    this.modalOpenSubject.next(true);
    console.log('openModal');
  }

  closeModal() {
    this.modalOpen = false;
    this.modalOpenSubject.next(false);
  }

  isModalOpen(): Observable<boolean> {
    return this.modalOpenSubject.asObservable();
  }

  getAgendas() {

    return this.http.get(api + '/articles/all', {headers: { 'Content-Type': 'application/json'}}).subscribe(
      (data) => {
        this.agenda = data as any;
        console.log(this.agenda);
      }
    )
  }

  getAgenda(id: string) {
    return this.http.get(api + '/agenda/' + id);
  }

  createArticle(article: any) {

    return this.http.post(api + '/articles/create', article).subscribe(
      (data) => {
        console.log(data);
      }, (error) => {
        if(error.status === 200) {
          this.closeModal();
          this.agenda.push(article);
        }
      }, () => {
        this.closeModal();
        this.agenda.push(article);
      }
    )
  }
}
