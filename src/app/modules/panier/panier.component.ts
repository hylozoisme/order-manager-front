import { Component } from '@angular/core';
import {PanierService} from "../../core/services/panier.service";

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent {

    constructor(public panierService: PanierService) { }
}
