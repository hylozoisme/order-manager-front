import { Component } from '@angular/core';
import { Article } from "../../shared/models/article";
import {AgendaService} from "../../core/services/agenda.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  constructor(public agendaService: AgendaService) {
    this.agendaService.getAgendas();
  }

  openModal() {
    this.agendaService.openModal();
  }
}
